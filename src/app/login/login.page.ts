import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  phone: number;
  otp: number;
  presentMode: string;
  constructor() {
    this.presentMode = 'continue';
  }

  ngOnInit() {
  }

  performEntryAction() {
    const tell = '+91' + this.phone;
    (window as any).FirebasePlugin.verifyPhoneNumber(tell, 60, (credential) => {
      console.log(credential);
      alert('Works');
    }, (error) => {
      console.error(error);
      alert(error);
     });
  }
}
